

class Outerclass5 {
    static void run(){
    System.out.println("in run");
    }
    class Innerclass5{
        Innerclass5(){
            System.out.println("in inner class ");
        }
    }
}
class client{
    public static void main(String[] args) {
        Outerclass5.run();
        Outerclass5.Innerclass5 obj=new Outerclass5().new Innerclass5();
    }
}
