class Outerclass {
    class Innerclass{
        void innermethod(){
            System.out.println("hello inner class");
        }
    }
    public static void main(String[] args) {
        Outerclass obj= new Outerclass();
        Innerclass obj1=obj.new Innerclass();
        obj1.innermethod();
    }
    
}
