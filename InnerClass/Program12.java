 class Outer12 {
    int x=10;
    static int y=20;
    Outer12(int x){
        System.out.println("in outer constructor");
        System.out.println(x);
        System.out.println(this.x);
    }
    class Inner12{
        int x=30;
        Inner12(int x){
            System.out.println("in inner class");
            System.out.println(x);
            System.out.println(Inner12.this.x);
            System.out.println(Outer12.this.x);
        }
    }
    public static void main(String[] args) {
        Outer12 outobj12=new Outer12(40);
        Inner12 obj12=outobj12.new Inner12(50);
    }
}
