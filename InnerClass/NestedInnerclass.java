

class NestedOuter {
    class Inner21{
        Inner21(){
            System.out.println("inner1 class constructor");
        }
        class Inner22{
            Inner22(){
                System.out.println("inner2 class ");
            }
        }
    }
    public static void main(String[] args) {
       /*  NestedOuter outobj=new NestedOuter();
        Inner21 obj1=outobj.new Inner21();
        Inner21.Inner22 obj2=obj1.new Inner22();*/
        Inner21.Inner22 obj=new NestedOuter().new Inner21().new Inner22();
    }
    
}
