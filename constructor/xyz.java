class xyz {
    void gun(){
        System.out.println("in gun");
    }
    void fun(int x){
        System.out.println("in fun");
        System.out.println(x);
    }
}
class Demo1{
    void run(float i,float f){
        System.out.println("in run");
        System.out.println(i);
        System.out.println(f);
    }
    public static void main(String[] args) {
        xyz obj1=new xyz();
        obj1.gun();
        obj1.fun(10);
        Demo1 obj=new Demo1();
        obj.run(50, 60);
    }
}
