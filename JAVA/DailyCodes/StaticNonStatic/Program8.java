class InputDemo8{
	void methodFun(){
		System.out.println("in fun function");
	}
	void methodGun(){
		System.out.println("in gun function");
	}
	void methodRun(){
		System.out.println("in run function");
	}
	public static void main(String[] args){
		System.out.println("in main method");
		InputDemo8 obj=new InputDemo8();
		obj.methodFun();
		obj.methodGun();
		obj.methodRun();
	}
}
