class Pattern9{
	public static void main(String[] args){
		char ch1='a';
		char ch2='A';
		for(int i=1;i<=9;i++){
			if(i%2==0){
				System.out.print(ch1++);
				ch2++;
			}else{
				System.out.print(ch2++);
				ch1++;
			}
		}
	}
}
