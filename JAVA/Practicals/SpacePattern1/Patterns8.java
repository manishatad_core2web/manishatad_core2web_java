import java.util.*;
class Spacepattern8{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		int num=1;
		for(int i=1;i<=row;i++){
			for(int sp=1;sp<i;sp++){
				System.out.print(" ");
			}
			
			for(int j=row;j>=i;j--){
				System.out.print(num++);
			}
			System.out.println();
			num=i+1;
		}
	}
}
