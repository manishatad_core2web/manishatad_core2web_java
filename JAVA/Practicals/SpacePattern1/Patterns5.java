import java.util.*;
class Spacepattern5{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int sp=1;sp<=row-i;sp++){
				System.out.print("  ");
			}
			for(int j=1;j<=i;j++){
				int num=row;
				System.out.print(num*j +" ");
			}
			System.out.println();
		}
	}
}
