import java.util.*;
class Spacepattern9{
	public static void main(String[] args){
		Scanner s=new Scanner(System.in);
		int row=s.nextInt();
		for(int i=1;i<=row;i++){
			for(int sp=1;sp<i;sp++){
				System.out.print(" ");
			}
			int ch=64+row;
			for(int j=row;j>=i;j--){
				System.out.print((char)ch-- );
			}
			System.out.println();
		}
	}
}
