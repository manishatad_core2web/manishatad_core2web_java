import java.util.*;
class Spacepattern3{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int sp=1;sp<i;sp++){
				System.out.print(" ");
			}
			int ch=64+row;
			for(int j=row;j>=i;j--){
				System.out.print((char)ch--);
			}
			System.out.println();
		}
	}
}

