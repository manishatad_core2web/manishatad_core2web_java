import java.util.*;
class Pattern9{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter row:");
		int row=sc.nextInt();
		int num=row*(row+1)/2;
		int x=num*2-1;
		for(int i=row;i>0;i--){
			for(int j=1;j<=i;j++){
				System.out.print(x +" ");
				x-=2;
			}
			System.out.println();
		}
	}
}

