import java.util.*;
class Pattern5{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter row");
		int row=sc.nextInt();
		for(int i=row;i>0;i--){
			char ch='A';
			char ch1='a';
			for(int j=1;j<=i;j++){
				if(i%2==0){
					System.out.print(ch++ +" ");
				}else{
					System.out.print(ch1++ +" ");
				}
			}
			System.out.println();
		}
	}
}

