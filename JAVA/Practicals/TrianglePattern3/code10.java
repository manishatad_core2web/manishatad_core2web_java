import java.util.*;
class Pattern10{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter rows");
		int row=sc.nextInt();
		for(int i=row;i>0;i--){
			int ch=64+i;
			int ch1=96+i;
			for(int j=1;j<=i;j++){
				if(i%2==1){
					System.out.print((char)ch1-- +" ");
					ch--;
				}else{
					System.out.print((char)ch-- +" ");
					ch1--;
			}
			}
			System.out.println();
		}
	}
}
