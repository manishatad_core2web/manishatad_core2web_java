import java.util.*;
class Pattern4{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter rows");
		int row=sc.nextInt();
		int num=row*(row+1)/2;
		int ch=num+64;
		for(int i=row;i>0;i--){
			for(int j=1;j<=i;j++){
				System.out.print((char)ch-- +" ");
			}
		System.out.println();
		}
	}
}
