import java.util.*;
class Pattern9{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter rows");
		int row=sc.nextInt();
		int num=1;
		int k=row;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=k;j++){
				System.out.print(num++ +" ");
			}
			num=1+i;
			k--;
			System.out.println();
		}
	}
}

