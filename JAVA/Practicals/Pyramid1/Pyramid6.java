import java.util.Scanner;
class Py6{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		
		for(int i=1;i<=row;i++){
			for(int sp=1;sp<=row-i;sp++){
				System.out.print(" "+"\t");
			}
			int num=row;
			for(int j=1;j<=i;j++){
				System.out.print(num-- +"\t");
			}
			for(int k=row-i+1;k<row;k++){
				System.out.print(k+1 +"\t");
			}
			System.out.println();
		}
	}
}
