import java.util.*;
class Py2{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();

		int num=1;
		for(int i=1;i<=row;i++){
			for(int sp=row;sp>i;sp--){
				System.out.print(" "+"\t");
			}
			for(int j=1;j<i*2;j++){
				System.out.print(num+"\t");
			}
			System.out.println();
		}
	}
}
