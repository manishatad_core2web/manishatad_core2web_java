import java.util.*;
class Py10{
	public static void  main(String[] args){
		Scanner sc=new Scanner(System.in);
		int row =sc.nextInt();
		int ch=64+row;
		for(int i=1;i<=row;i++){
			
			for(int sp=1;sp<=row-i;sp++){
				System.out.print(" "+"\t");
			}
			for(int j=1;j<=i;j++){
				System.out.print((char)ch++ +"\t");
				
			}
			ch-=2;
			for(int k=1;k<=i-1;k++){
				System.out.print((char)ch-- +"\t");
			}
			System.out.println();
			ch=64+row-i;
		}
	}
}

