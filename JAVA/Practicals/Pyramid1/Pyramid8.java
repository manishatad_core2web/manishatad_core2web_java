import java.util.*;
class Py8{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int sp=1;sp<=row-i;sp++){
				System.out.print(" "+"\t");
			}
			for(int j=1;j<i*2;j++){
				System.out.print(i+"\t");
			}
			System.out.println();
		}
	}
}
