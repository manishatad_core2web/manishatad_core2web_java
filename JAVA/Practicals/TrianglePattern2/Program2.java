import java.util.*;
class Pattern2{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter rows:");
		int row=sc.nextInt();
		
		for(int i=1;i<=row;i++){
			char ch='A';
			for(int j=1;j<=i;j++){
				
				if(i%2==0){
					System.out.print("$  ");
				}else{
					System.out.print(ch+" ");
					ch++;
				}
			}
				System.out.println();
			
		}
	}
}

