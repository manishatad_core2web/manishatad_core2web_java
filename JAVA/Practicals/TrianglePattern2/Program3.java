import java.util.*;
class Pattern3{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter row:");
		int row=sc.nextInt();

		for(int i=1;i<=row;i++){
			int ch=64+row;
			for(int j=1;j<=i;j++){
				System.out.print((char)ch+" ");
				ch--;
			}
			System.out.println();
		}
	}
}

