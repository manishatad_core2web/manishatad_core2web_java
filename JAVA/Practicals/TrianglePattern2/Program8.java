import java.util.*;
class Pattern8{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter rows:");
		int row=sc.nextInt();
		char ch='a';
		for(int i=1;i<=row;i++){
			int num=1;
			for(int j=1;j<=i;j++){
				if(j%2==1){
					System.out.print(num++ +" ");
					num+=j;
					ch++;
				}else{
					System.out.print(ch++ +" ");
					
				}
			}
			System.out.println();
			}
	}
}
