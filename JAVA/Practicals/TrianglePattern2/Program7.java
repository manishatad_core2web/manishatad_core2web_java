import java.util.*;
class Pattern7{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter row:");
		int row=sc.nextInt();
		int num=1;
		char ch='A';
	
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if(j%2==0){
					System.out.print(ch++ +" ");
				}else{
					System.out.print(num +" ");
				}
			}
			num++;
			System.out.println();

		}
	}
}

