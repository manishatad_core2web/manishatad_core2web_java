import java.util.*;
class Pattern10{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter row:");
		int row=sc.nextInt();
		int num=1;
		char ch='a';
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if(i%2==1){
					System.out.print(num++ +" ");
					ch++;
				}else{
					System.out.print(ch++ +" ");
					num++;
				}
			}System.out.println();
		}
	}
}

