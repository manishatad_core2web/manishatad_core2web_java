import java.util.*;
class Pattern4{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter row:");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int ch=64+row;
			int chlow=96+row;
			for(int j=1;j<=i;j++){
				if(i%2==1){
					System.out.print((char)chlow+" ");
					chlow--;
				}else{
					System.out.print((char)ch+ " ");
					ch--;
				}
			}
			System.out.println();
		}
	}
}

