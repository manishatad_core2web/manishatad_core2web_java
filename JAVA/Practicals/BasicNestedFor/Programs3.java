import java.util.*;
class ForDemo3{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Rows:");
		int x=sc.nextInt();
		int y=1;
		for(int i=1;i<=x;i++){
			for(int j=1;j<=x;j++){
				System.out.print(y+" ");
			}
			System.out.println();
			++y;
		}
	}
}
