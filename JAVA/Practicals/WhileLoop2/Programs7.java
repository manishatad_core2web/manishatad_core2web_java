import java.util.*;
class While7{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter number");
		int num=sc.nextInt();
		int sum=0;
		while(num>0){
			int rem=num%10;
			if(rem%2==0){
				sum=sum+rem;
			}
			num/=10;
		}
		System.out.println(sum);
	}
}
