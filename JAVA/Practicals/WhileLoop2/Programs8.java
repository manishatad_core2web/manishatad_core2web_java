import java.util.*;
class While8{
	public static void main(String[] args){
		Scanner sc =new Scanner(System.in);
		int prod=1;
		System.out.println("number:");
		int num=sc.nextInt();
		while(num>0){
			int rem=num%10;
			if(rem%2==1){
				prod=prod*rem;
			}
			num/=10;
		}
		System.out.println(prod);
	}
}
