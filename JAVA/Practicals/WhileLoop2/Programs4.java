import java.util.*;
class While4{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter number :");
		int num=sc.nextInt();
		while(num>0){
			int rem=num%10;
			if(rem%2==1){
				System.out.println(rem*rem);
			}
			num/=10;
		}
	}
}
