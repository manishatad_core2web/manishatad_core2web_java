import java.util.*;
class While5{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter number:");
		int num=sc.nextInt();
		while(num>0){
			int rem=num%10;
			if(rem%2==0){
				System.out.println(rem*rem*rem);
			}
			num/=10;
		}
	}
}
