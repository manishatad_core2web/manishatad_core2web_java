import java.util.*;
class While9{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter number");
		int num=sc.nextInt();
		int sum=0;
		while(num>0){
			int rem=num%10;
			if(rem%2==1){
				sum=sum+(rem*rem);
			}
			num/=10;
		}System.out.println(sum);
	}
}
