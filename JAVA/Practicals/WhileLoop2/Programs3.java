import java.util.*;
class While3{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter number:");
		int num=sc.nextInt();
		while(num>0){
			int rem=num%10;
			if(rem%2==0|| rem%3==0){
				System.out.println(rem);
			}
			num/=10;
		}
	}
}
