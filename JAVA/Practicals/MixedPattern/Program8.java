import java.util.*;
class Pattern8{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter row:");
		int row=sc.nextInt();
		
		int num=row*(row+1)/2;
		int ch=64+num;
		for(int i=row;i>0;i--){
			for(int j=1;j<=i;j++){
				System.out.print((char)(ch)+" ");
				ch--;
			}
			System.out.println();
			
		}
	}
}

