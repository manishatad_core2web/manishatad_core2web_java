import java.util.*;
class MixedPattern7{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter row:");
		int row= sc.nextInt();
		int num=row;
		for(int i=1;i<=row;i++){
			for(int sp=1;sp<=row-i;sp++){
				System.out.print(" "+"\t");
			}
			for(int j=1;j<i*2;j++){
				System.out.print(num+"\t");
				num++;
			}	
			num=row-i;
		
		System.out.println();
	}
}}
