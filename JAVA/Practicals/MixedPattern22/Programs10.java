import java.util.*;
class MixedPattern10{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("enter row:");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int sp=1;sp<=row-i;sp++){
				System.out.print(" "+"\t");
			}
			int num=i;
			for(int j=1;j<=i;j++){
				System.out.print(num--+" \t");
			}
			num++;
			for(int k=i-1;k>=1;k--){
				System.out.print(++num +"\t");
			}
			System.out.println();
		}
	}
}
