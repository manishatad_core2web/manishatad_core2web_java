import java.util.*;
class MixedPattern8{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("enter row:");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int sp=1;sp<=row-i;sp++){
				System.out.print(" "+"\t");
			}
			int num=i*2-1;
			for(int j=1;j<i*2;j++){
				System.out.print(num+" \t");
				num--;
			}
			System.out.println();
		}
	}
}

