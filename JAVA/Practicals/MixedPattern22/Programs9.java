import java.util.*;
class MixedPattern9{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("enter row");
		int row=sc.nextInt();
		int num=row;
		for(int i=1;i<=row;i++){
			for(int sp=1;sp<=row-i;sp++){
				System.out.print(" "+" \t");
			}
			for(int j=1;j<i*2;j++){
				System.out.print(num+" \t");
				num--;
			}
			System.out.println();
			num=row+i;
		}
	}
}
