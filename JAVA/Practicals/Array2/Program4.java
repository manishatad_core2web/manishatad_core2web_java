import java.util.*;
class ArrayDemo4{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter size of array:");
		int size=sc.nextInt();
		int [] arr=new int[size];
		System.out.println("enter element of Array:");
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("enter number to search in array:");
		int search=sc.nextInt();
		int index=-1;
		for(int i=0;i<size;i++){
			if(arr[i]==search){
				index=i;
				break;
			}
		}
		if(index!=-1){
			System.out.println(search+"found at index" + index);
		}else{
			System.out.println(search+"not found in array");
		}
	}
}
