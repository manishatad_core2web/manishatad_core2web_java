import java.util.*;
class Square6{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter row:");
		int row =sc.nextInt();
		int num=row;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(num%2==1){
					System.out.print(num*num +"\t");
					num++;
				}else{
					System.out.print(num++ +"\t");
				}
			}
			System.out.println();
		}
	}
}

