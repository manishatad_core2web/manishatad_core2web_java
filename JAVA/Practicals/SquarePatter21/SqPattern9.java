import java.util.*;
class Square9{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter row:");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				int val=i+j+1;
				if(val%2==0){
					System.out.print("2");
				}
				else{
					System.out.print(val+" ");
				}
			}
			System.out.println();
		}
	}
}
