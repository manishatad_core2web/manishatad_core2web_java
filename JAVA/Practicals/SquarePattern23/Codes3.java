import java.util.*;
class Pattern3{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();

		int ch=64+row;
		int num=row;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if (num%2==1){
					System.out.print((char)ch+" ");
				}else{
					System.out.print(num+" ");
			}
			num++;
			ch++;
		}

		
		System.out.println();
	}
	}

}

