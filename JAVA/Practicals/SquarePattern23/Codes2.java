import java.util.*;
class Pattern2{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
			int row=sc.nextInt();
			int ch1=64+row;
			int ch2=96+row;
			for(int i=1;i<=row;i++){
				for(int j=1;j<=row;j++){
					if(j==row){
						System.out.print((char)ch1+" ");
					}
					else{
						System.out.print((char)ch2+" ");
					}
					ch1++;
					ch2++;
				}
				System.out.println();
			}
		}
	}
