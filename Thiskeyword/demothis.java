class demothis {
    int x=10;
    demothis(){
        this(10);
        System.out.println("in no argument");
    }
    demothis(int x){
        this(10,20);
        System.out.println("1 argument");
    }
    demothis(int x,int y){
    
        System.out.println(this.x);
        System.out.println("2 argument");
    }
    public static void main(String[] args) {
        demothis d=new demothis();
    }
}
