class This4 {
    int x=10;
    This4(int x){
        System.out.println(x);
        x=this.x;
        System.out.println(x);
        this.x=x;
    }
    void run(){
        System.out.println(x);
    }
    public static void main(String[] args) {
        This4 obj=new This4(20);
        obj.run();
    }

    
}
