public class Overriding1 {
    void career( ){
        System.out.println("Teacher");
    }
    void marray( ){
        System.out.println("tad");
    }
}
class Child extends Overriding1{
    void career( ){
        System.out.println("Engineer");
    }
    void marray( ){
        System.out.println("manisha");
    }
}
class client{
    public static void main(String[ ] args) {
        Overriding1 obj=new Child( );
        obj.career( );
        obj.marray( );
    }
}
